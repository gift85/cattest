from django.db import models

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length = 100)
    categories = models.ManyToManyField('Category', through='Relation')

class Category(models.Model):
    name = models.CharField(max_length = 100)

class Relation(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)