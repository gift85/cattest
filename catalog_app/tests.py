from django.test import TestCase

# Create your tests here.

from .models import Product, Category


class ProductsMethodTest(TestCase):

    #wrong test!
    def test_products_with_categories(self):

        categories = Category.objects.values('id')
        product_list = Product.objects.filter(categories__id__in=categories).distinct().count()
        product_count = Category.objects.all().count()
        self.assertEqual(product_list, product_count)