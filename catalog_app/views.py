from django.shortcuts import render

# Create your views here.

from .models import Product


def products(request):
    if 'categories' in request.GET:
        cats = request.GET['categories']
        cats_list = cats.split(',')
        product_list = Product.objects.filter(categories__id__in=cats_list).distinct()
    else:
        product_list = Product.objects.all()

    if 'offset' in request.GET:
        offset = int(request.GET['offset'])
        product_list = product_list.all()[offset:]

    if 'limit' in request.GET:
        limit = int(request.GET['limit'])
        product_list = product_list.all()[:limit]

    context = {
        'product_list': product_list,
    }

    return render(request, 'catalog_app/products.html', context)